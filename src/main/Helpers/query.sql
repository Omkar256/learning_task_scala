CREATE TABLE Brand (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name varchar(255) UNIQUE,
    Timestamp TIMESTAMP,
    URL varchar(255) UNIQUE,
    is_active BOOLEAN
);

CREATE TABLE Category (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name varchar(255) UNIQUE,
    Timestamp TIMESTAMP,
    URL varchar(255) UNIQUE,
    is_active BOOLEAN
);

CREATE TABLE Product (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name varchar(255) UNIQUE,
    MRP INT,
    our_price INT,
    Timestamp TIMESTAMP,
    URL varchar(255) UNIQUE,
    is_active BOOLEAN,
    brand_id INT,
    FOREIGN KEY (brand_id) REFERENCES Brand (ID),
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES Category (ID)
);

CREATE TABLE User (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    Name varchar(255),
    phone_number INT,
    state varchar(255),
    city varchar(255),
    pincode INT,
    Timestamp TIMESTAMP,
    is_active BOOLEAN
);

CREATE TABLE Order_order (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    total_mrp_amount INT,
    total_our_price INT,
    Timestamp TIMESTAMP,
    Status varchar(255),
    FOREIGN KEY (user_id) REFERENCES User (ID)
);

CREATE TABLE Order_items (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    order_id INT,
    product_id INT,
    mrp INT,
    our_price INT,
    quantity INT,
    FOREIGN KEY (order_id) REFERENCES Order_order (ID),
    FOREIGN KEY (product_id) REFERENCES Product (ID)
);