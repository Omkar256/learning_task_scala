from datetime import timezone
from datetime import datetime
import random
import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',
                                         database='purplle',
                                         user='root',
                                         password='password')
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

        # Brand and Catogory
        # for i in range(1, 21): 
        #     cursor.execute(f"INSERT INTO Brand VALUES ({i}, 'Brand_Name_{i}', CURRENT_TIMESTAMP, 'Brand_URL_{i}', TRUE);")
        #     cursor.execute(f"INSERT INTO Category VALUES ({i}, 'Category_Name_{i}', CURRENT_TIMESTAMP, 'Cateogory_URL_{i}', TRUE);")

        # # Users
        # for i in range(1, 1001):
        #     cursor.execute(f"INSERT INTO User VALUES ({i}, 'User_Name_{i}', {random.randint(1e8, 1e9-1)}, {random.randint(1, 29)}, {random.randint(1, 200)}, {random.randint(400000, 400099)}, CURRENT_TIMESTAMP, TRUE);")

        # Products
        products = []
        for i in range(1, 501):
            MRP = random.randint(1, 1000)
            price = random.randint(1, MRP)
            products.append([MRP, price])
            cursor.execute(f"INSERT INTO Product VALUES ({i}, 'Product_Name_{i}', {MRP}, {price}, CURRENT_TIMESTAMP, 'Product_URL_{i}', TRUE, {random.randint(1, 20)}, {random.randint(1, 20)});")

        # Order and Order Items
        for i in range(1, 101):
            print(f"\r{i}", end = "")
            totalItems = random.randint(1, 10)
            totalMRP = 0
            totalPrice = 0
            seen = set()
            order_items_queries = []
            for j in range(1, totalItems+1):
                productID = random.randint(1, 500)
                while productID in seen:
                    productID = random.randint(1, 500)
                seen.add(productID)
                quantity = random.randint(1, 4)
                totalPrice += products[productID-1][1]*quantity
                totalMRP += products[productID-1][0]*quantity
                order_items_queries.append(f"INSERT INTO Order_items VALUES (NULL, {i}, {productID}, {products[productID-1][0]}, {products[productID-1][1]}, {quantity});")
            
            cursor.execute(f"INSERT INTO Order_order VALUES ({i}, {random.randint(1, 1000)}, {totalMRP}, {totalPrice}, CURRENT_TIMESTAMP, {random.randint(1, 5)});")
            for query in order_items_queries:
                cursor.execute(query)

    connection.commit()

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")