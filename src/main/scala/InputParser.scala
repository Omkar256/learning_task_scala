import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

object InputParser {
  def getInput(): (DataFrame, DataFrame, DataFrame, DataFrame, DataFrame, DataFrame) = {

    val brand_df = getTableFromSQL("Brand")
    val category_df = getTableFromSQL("Category")
    val product_df = getTableFromSQL("Product")
    val user_df = getTableFromSQL("User")
    val order_df = getTableFromSQL("Order_order")
    val order_items_df = getTableFromSQL("Order_items")

    return (brand_df, category_df, product_df, user_df, order_df, order_items_df)
  }

  def getTableFromSQL(tableName: String) : DataFrame = {
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Learning Task")
      .getOrCreate()

    val df = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://localhost:3306/purplle")
      .option("dbtable", tableName)
      .option("user", "root")
      .option("password", "password")
      .load()

    return df
  }
}