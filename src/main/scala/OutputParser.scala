import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

object OutputParser {
  def putSQLOutput(df: DataFrame, name: String)= {

    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Learning Task")
      .getOrCreate()

    df.write
      .format("jdbc")
      .option("url", "jdbc:mysql://localhost:3306/purplle")
      .option("dbtable", name)
      .option("user", "root")
      .option("password", "password")
      .mode("overwrite")
      .save()
  }
}