import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, functions}

object Transformations {

  def business_transform(brand_df: DataFrame, category_df: DataFrame, product_df: DataFrame, user_df: DataFrame, order_df: DataFrame, order_items_df: DataFrame): (DataFrame, DataFrame) = {
    val order_details_df = order_df
      .drop("total_our_price")
      .drop("total_mrp_amount")
      .drop("Status")
      .join(order_items_df.drop("mrp").drop("our_price"), order_df("ID") === order_items_df("order_id"))
      .drop("ID")
      .join(product_df, product_df("ID") === order_items_df("product_id"))
      .drop("ID")
      .drop("is_active")
      .drop("brand_id")
      .drop(product_df("Timestamp"))
      .join(user_df
        .select("ID", "state", "city", "pincode"),
        user_df("ID") === order_df("user_id"))
      .drop("ID")

    val final_order_details_df = order_details_df.withColumnRenamed("Timestamp", "order_timestamp")
      .withColumnRenamed("product_id", "orderitem_product_id")
      .withColumnRenamed("MRP", "orderitem_product_mrp")
      .withColumnRenamed("our_price", "orderitem_product_our_price")
      .withColumnRenamed("quantity", "orderitem_product_quantity")
      .withColumnRenamed("state", "user_state")
      .withColumnRenamed("city", "user_city")
      .withColumnRenamed("pincode", "user_pincode")

    final_order_details_df.show()

    val uniq_products = final_order_details_df.groupBy("order_id")
      .agg(functions.countDistinct("orderitem_product_id").alias("unique product count"))

    val uniq_category = final_order_details_df.groupBy("order_id")
      .agg(functions.countDistinct("category_id").alias("unique category count"))

    val uniq_brand = final_order_details_df.join(product_df.select("ID", "brand_id"), product_df("ID") === final_order_details_df("orderitem_product_id"))
      .groupBy("order_id")
      .agg(functions.countDistinct("brand_id").alias("unique brand count"))

    val product_brand_df = brand_df.join(product_df.withColumnRenamed("ID", "product_id").drop("Name"), product_df("brand_id") === brand_df("ID"))
      .withColumnRenamed("Name", "brand_Name")

    val uniq_brand_name = final_order_details_df.join(product_brand_df, final_order_details_df("orderitem_product_id") === product_brand_df("product_id"))
      .groupBy("order_id")
      .agg(functions.concat_ws(",", functions.collect_set("brand_name")).alias("brand_name_csv"))

    val uniq_category_name = final_order_details_df.join(category_df.alias("category"), category_df("ID") === final_order_details_df("category_id"))
      .groupBy("order_id")
      .agg(functions.concat_ws(",", (functions.collect_set("category.Name"))).alias("category name_csv"))

    val total_product = final_order_details_df.groupBy("order_id")
      .agg(functions.sum("orderitem_product_quantity").alias("total product count"))

    val total_prices = final_order_details_df
      .withColumn("total_prices", col("orderitem_product_our_price") * col("orderitem_product_quantity"))
      .withColumn("total_mrp_prices", col("orderitem_product_mrp") * col("orderitem_product_quantity"))

    val avg_product_price = total_prices.groupBy("order_id")
      .agg((functions.sum("total_prices") / functions.sum("orderitem_product_quantity")).alias("avg product price"))

    val total_discount = total_prices.groupBy("order_id")
      .agg((functions.sum("total_mrp_prices") - functions.sum("total_prices")).alias("total discount"))

    val order_stats_df = uniq_products.join(uniq_brand, Seq("order_id"))
      .join(uniq_category, Seq("order_id"))
      .join(uniq_brand_name, Seq("order_id"))
      .join(uniq_category_name, Seq("order_id"))
      .join(total_product, Seq("order_id"))
      .join(avg_product_price, Seq("order_id"))
      .join(total_discount, Seq("order_id"))

    order_stats_df.show(truncate = false)

    return (order_details_df, order_stats_df)
  }
}