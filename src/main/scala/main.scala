import org.apache.spark.sql.SparkSession

object Main {
  def main(arg: Array[String]):Unit = {

    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Learning Task")
      .getOrCreate()

    val (brand_df, category_df, product_df, user_df, order_df, order_items_df) = InputParser.getInput()

    val (order_details_df, order_stats_df) = Transformations.business_transform(brand_df, category_df, product_df, user_df, order_df, order_items_df)

    OutputParser.putSQLOutput(order_details_df, "Order_details")
    OutputParser.putSQLOutput(order_stats_df, "Order_stats")
  }


}
